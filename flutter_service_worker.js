'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "assets/AssetManifest.json": "213d0b54a54ddb6a92faa06459074bee",
"assets/assets/images/logo.png": "8152c1b582bc4c170d49b1e69dd0fb9b",
"assets/assets/images/splash_1.png": "51c064d3f19cf39aee627a3aed744193",
"assets/assets/images/splash_2.png": "3580b1c0bb9f244b51d8042b8e762bf5",
"assets/assets/images/splash_3.png": "54b2889511d311463cf331284d9197bb",
"assets/FontManifest.json": "dc3d03800ccca4601324923c0b1d6d57",
"assets/fonts/MaterialIcons-Regular.otf": "1288c9e28052e028aba623321f7826ac",
"assets/NOTICES": "cb51cb7cdad58cc3975b6e3000ef45cc",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "b14fcf3ee94e3ace300b192e9e7c8c5d",
"assets/packages/js_store_ui/assets/icons/arrow_left.svg": "16b5dc0d8535314b97e16f7b2ce8cf4a",
"assets/packages/js_store_ui/assets/icons/arrow_right.svg": "7d2e92d8f069ee2c73f1820beea62dfd",
"assets/packages/js_store_ui/assets/icons/back_icon.svg": "a1540761ddc4d5ebd1ebf7e732a7af0a",
"assets/packages/js_store_ui/assets/icons/Bell.svg": "03c0fc05f0c26d3107496511ef140dd2",
"assets/packages/js_store_ui/assets/icons/bill_icon.svg": "c14fba2dc1da1beac210bc96c90568a6",
"assets/packages/js_store_ui/assets/icons/Call.svg": "52af54660badff2b15a84fd34203c7ee",
"assets/packages/js_store_ui/assets/icons/camera_icon.svg": "25207a856bce06a7c1f4d695638a0383",
"assets/packages/js_store_ui/assets/icons/cart_icon.svg": "220f5aa89905b1351c4fdf71aee29f11",
"assets/packages/js_store_ui/assets/icons/Cash.svg": "9a1bfa4f03729b332bb98439f713637e",
"assets/packages/js_store_ui/assets/icons/Categories.svg": "4bc439e1403efbb549bfeb70bb86f750",
"assets/packages/js_store_ui/assets/icons/chat_bubble_Icon.svg": "3e6e1ab6925d3db86ce9bd109c121db0",
"assets/packages/js_store_ui/assets/icons/check_mark_rounde.svg": "e83caa6ca41e8af9229ae29d79f9410b",
"assets/packages/js_store_ui/assets/icons/Close.svg": "70478a15e0479750b0b03e000041e9f3",
"assets/packages/js_store_ui/assets/icons/Conversation.svg": "291fb6c6acdeb807afb452ae90f930db",
"assets/packages/js_store_ui/assets/icons/Discover.svg": "0981b6e74080b318d0bb4255681d2e05",
"assets/packages/js_store_ui/assets/icons/Error.svg": "0f876e9b9170982e37bbf767c6ebb47f",
"assets/packages/js_store_ui/assets/icons/facebook-2.svg": "48bf15d2057966765f384827997a2f41",
"assets/packages/js_store_ui/assets/icons/flash_icon.svg": "8b2bc2fd2894d143940eb565b4a827fe",
"assets/packages/js_store_ui/assets/icons/game_icon.svg": "f913dbd8d0e9e437cfb53d774eb980d9",
"assets/packages/js_store_ui/assets/icons/gift_icon.svg": "6feeb93f33263146741e38f4ebfbccbc",
"assets/packages/js_store_ui/assets/icons/google-icon.svg": "38e282dafbaaf9823263d49349670447",
"assets/packages/js_store_ui/assets/icons/heart_icon.svg": "a2be4f5f2079f5ff3dbd117eeaa9471e",
"assets/packages/js_store_ui/assets/icons/heart_icon_2.svg": "f728c6b3d75dfab6450f56a2a57633f0",
"assets/packages/js_store_ui/assets/icons/location_point.svg": "146ea387710fa420046c0f71b89ad474",
"assets/packages/js_store_ui/assets/icons/Lock.svg": "44a5fb6d11a48fd52c87d95e34e7a689",
"assets/packages/js_store_ui/assets/icons/log_out.svg": "ef8dc3d612e3e63ae4728a24d9982f13",
"assets/packages/js_store_ui/assets/icons/Mail.svg": "6b876f2539a1946b1a946e7a5646e909",
"assets/packages/js_store_ui/assets/icons/Parcel.svg": "45a2e4513281a1c511cff0d7d86439d1",
"assets/packages/js_store_ui/assets/icons/Phone.svg": "589731a88a098c9c6d40e32bc11c3d83",
"assets/packages/js_store_ui/assets/icons/plus_icon.svg": "23bd873f0fdef239500d68a150f9fa93",
"assets/packages/js_store_ui/assets/icons/question_mark.svg": "7d0f74b3eb3cbeac772cb6a41476cfcf",
"assets/packages/js_store_ui/assets/icons/receipt.svg": "e0ecaf4c17597903fa1e8ab3c28fa963",
"assets/packages/js_store_ui/assets/icons/remove.svg": "77f17bcf86cb62db1b3ce224d6cb6fd3",
"assets/packages/js_store_ui/assets/icons/search_icon.svg": "5383aff67a0cc61bc20b953c73d87469",
"assets/packages/js_store_ui/assets/icons/Settings.svg": "d8fd4b8ed70a516c17d3d981d9a49999",
"assets/packages/js_store_ui/assets/icons/shop_icon.svg": "1eda40840728635d3279f313774d1675",
"assets/packages/js_store_ui/assets/icons/star_icon.svg": "1ef6ad3bbe15947a5b4d9bf153101fd3",
"assets/packages/js_store_ui/assets/icons/Success.svg": "b0a226cdd68878cf33bddc8d9d2cc1f6",
"assets/packages/js_store_ui/assets/icons/Trash.svg": "f877c275194b39ca5f21fe7202ca852a",
"assets/packages/js_store_ui/assets/icons/twitter.svg": "2186bb91925602b76e5a4384b2198c06",
"assets/packages/js_store_ui/assets/icons/User.svg": "737d49c1953f8098f618d523b45e5657",
"assets/packages/js_store_ui/assets/icons/user_icon.svg": "950d2f1652bdb59675231707a9124535",
"assets/packages/js_store_ui/assets/images/apple-pay.png": "8ed30196e39ee689aa2cc5b604da80ee",
"assets/packages/js_store_ui/assets/images/fedex-express.png": "5c65ce272dc58c3dd0b416f0549f23e4",
"assets/packages/js_store_ui/assets/images/google-pay.png": "515fdb5d6b0c72ab7ff1d020715da990",
"assets/packages/js_store_ui/assets/images/mastercard-2.png": "03c20b63d4ffa13484f48fac9d2e32b2",
"assets/packages/js_store_ui/assets/images/no-image-placeholder.png": "72699f1fe56c6114f6f721ee9bc8b4ff",
"assets/packages/js_store_ui/assets/images/paypal.png": "fdd28064849926e343b82d25e9df0a03",
"assets/packages/js_store_ui/assets/images/splash_1.png": "51c064d3f19cf39aee627a3aed744193",
"assets/packages/js_store_ui/assets/images/splash_2.png": "3580b1c0bb9f244b51d8042b8e762bf5",
"assets/packages/js_store_ui/assets/images/splash_3.png": "54b2889511d311463cf331284d9197bb",
"assets/packages/js_store_ui/assets/images/visa.png": "e5102f3cf10261b0cf2ddb4c27e66f14",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"images/logo.png": "dc35fe712bafa278cf90edb60cc07842",
"index.html": "5b9f3434fc802b030958e2baadbb1418",
"/": "5b9f3434fc802b030958e2baadbb1418",
"main.dart.js": "f3faacd509f17964c1eca6ab0e91338d",
"manifest.json": "9b3b5d1bce887eae3397f9120f8cad65",
"version.json": "1bf31c09e930da358f6e25b5279c5243"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "/",
"main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value + '?revision=' + RESOURCES[value], {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache.
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
